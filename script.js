const botao = document.querySelector(".botao-enviar")

botao.addEventListener("click", (event) => {
    const mensagens = document.querySelector(".mensagens")
    const txtArea = document.querySelector(".texto")
    

    const li = document.createElement("li")
    li.classList.add("mensagem")

    li.innerHTML = `<p class="texto-msg">${txtArea.value}</p>
            <div class="botoes">
                <input type="button" value="Deletar" class="Deletar botao-msg" onclick="deletar(this)">
            </div>`

    txtArea.value = ""
    mensagens.appendChild(li) 
})

function deletar(elemento) {
    const parent = elemento.parentElement.parentElement    
    parent.remove()
}